//
//  NetManager.m
//  Contacts
//
//  Created by Павел on 03/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import "NetManager.h"

@interface NetManager ()
@property(nonatomic, strong) NSURLSession *session;
@end

@implementation NetManager

static NSString *const BASE_URL = @"https://contact.taxsee.com/Contacts.svc/%@?login=%@&password=%@";
static NSString *const METHOD_HELLO = @"Hello";
static NSString *const METHOD_DATA = @"GetAll";
static NSString *const METHOD_PHOTO = @"GetWPhoto";

+ (instancetype)initWithUsername:(NSString *)username andPassword:(NSString *)password {
    NetManager *nm = [NetManager new];
    nm.username = username;
    nm.password = password;
    return nm;
}

- (NSURLSession *)session {
    if (!_session)
        _session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    return _session;
}

- (NSURL *)buildUrl:(NSString *)method {
    NSString *urlString = [NSString stringWithFormat:BASE_URL, method, self.username, self.password];
    return [NSURL URLWithString:urlString];
}

- (NSURL *)buildUrl:(NSString *)method withParams:(NSDictionary *)params {
    NSURLComponents *components = [NSURLComponents componentsWithURL:[self buildUrl:method] resolvingAgainstBaseURL:NO];
    NSMutableArray<NSURLQueryItem *> *items = [[NSMutableArray alloc] initWithArray:components.queryItems copyItems:YES];
    for (NSString *key in [params allKeys]) {
        NSURLQueryItem *item = [NSURLQueryItem queryItemWithName:key value:[params objectForKey:key]];
        [items addObject:item];
    }
    components.queryItems = items;
    return components.URL;
}

- (void)hello:(void (^)(BOOL result, NSString *message))completion {
    NSURL *url = [self buildUrl:METHOD_HELLO];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        if (!data) {
            return;
        }
        NSError *jsonError;
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
        if (jsonError) {
            return;
        }
        BOOL result = [[jsonData valueForKey:@"Success"] boolValue];
        NSString *message = [jsonData valueForKey:@"Message"];
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(result, message);
        });
    });
}

- (void)getData:(void (^)(NSMutableArray<Department *> *, NSError *error))completion {
    NSURL *url = [self buildUrl:METHOD_DATA];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        if (!data) {
            return;
        }
        NSError *jsonError;
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
        if (jsonError) {
            return;
        }

        NSMutableArray *result = [[NSMutableArray alloc] init];
        Department *department = [[Department alloc] init:jsonData[@"Id"] withName:jsonData[@"Name"] level:@0];

        if (jsonData[@"Departments"] != nil) {
            department.departments = [self parseDepartment:jsonData[@"Departments"] withLevel:@1];;
        }

        [result addObject:department];

        dispatch_async(dispatch_get_main_queue(), ^{
            completion(result, nil);
        });
    });
}

- (NSMutableArray<Department *> *)parseDepartment:(NSMutableArray *)jsonArray withLevel:(NSNumber *)level {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (NSDictionary *item in jsonArray) {
        Department *department = [[Department alloc] init:item[@"Id"] withName:item[@"Name"] level:level];
        if (item[@"Departments"] != nil) {
            department.departments = [self parseDepartment:item[@"Departments"] withLevel:@([level intValue] + 1)];
        }
        if (item[@"Employees"] != nil) {
            department.employees = [self parseEmployee:item[@"Employees"] withLevel:@([level intValue] + 1)];
        }
        [result addObject:department];
    }
    return result;
}

- (NSMutableArray<Employee *> *)parseEmployee:(NSMutableArray *)jsonArray withLevel:(NSNumber *)level {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (NSDictionary *item in jsonArray) {
        Employee *employee = [[Employee alloc] initFromDictionary:item withLevel:level];
        employee.photoUrl = [self buildUrl:METHOD_PHOTO withParams:@{@"id": employee.id}];
        [result addObject:employee];
    }
    return result;
}

@end
