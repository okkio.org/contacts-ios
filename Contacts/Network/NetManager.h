//
//  NetManager.h
//  Contacts
//
//  Created by Павел on 03/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Models/Department/Department.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetManager : NSObject
@property(nonatomic, copy) NSString *username;
@property(nonatomic, copy) NSString *password;

+ (instancetype)initWithUsername:(NSString *)username andPassword:(NSString *)password;

- (void)hello:(void (^)(BOOL result, NSString *message))completion;

- (void)getData:(void (^)(NSMutableArray<Department *> *, NSError *error))completion;
@end

NS_ASSUME_NONNULL_END
