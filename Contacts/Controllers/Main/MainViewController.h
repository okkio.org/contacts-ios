//
//  ViewController.h
//  Contacts
//
//  Created by Павел on 02/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetManager.h"
#import "LoginViewController.h"
#import "DetailViewController.h"

@interface MainViewController : UIViewController
+ (id)initWithIdentifier:(UIStoryboard *)storyboard;
@end

