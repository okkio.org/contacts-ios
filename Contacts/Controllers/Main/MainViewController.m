//
//  ViewController.m
//  Contacts
//
//  Created by Павел on 02/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()
- (IBAction)logout:(id)sender;

@property(nonatomic, weak) IBOutlet UITableView *uiTableView;
@property(nonatomic, strong) NSMutableArray *treeData;
@end

@implementation MainViewController

static NSString *const VIEW_IDENTIFIER = @"mainView";

+ (id)initWithIdentifier:(UIStoryboard *)storyboard {
    MainViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:VIEW_IDENTIFIER];
    return viewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.title = NSLocalizedString(@"Contacts", @"");

    // setup nivagation bar
    self.navigationController.navigationBar.prefersLargeTitles = YES;
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAutomatic;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];

    NSString *username = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"password"];

    self.treeData = [[NSMutableArray alloc] init];

    NetManager *nm = [NetManager initWithUsername:username andPassword:password];
    [nm getData:^(NSMutableArray<Department *> *result, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.treeData addObjectsFromArray:result];
            [self.uiTableView reloadData];
        });
    }];
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.treeData count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }

    NSInteger level = 0;
    NSString *title = @"";
    NSString *subTitle = @"";
    if ([[self.treeData objectAtIndex:indexPath.row] isKindOfClass:[Department class]]) {
        Department *department = (Department *) [self.treeData objectAtIndex:indexPath.row];
        title = department.name;
        level = [department.level integerValue];
    } else if ([[self.treeData objectAtIndex:indexPath.row] isKindOfClass:[Employee class]]) {
        Employee *employee = (Employee *) [self.treeData objectAtIndex:indexPath.row];
        title = employee.name;
        level = [employee.level integerValue];
        subTitle = employee.title;
    }

    cell.detailTextLabel.text = subTitle;
    cell.textLabel.text = title;
    cell.indentationLevel = level;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([[self.treeData objectAtIndex:indexPath.row] isKindOfClass:[Department class]]) {
        Department *department = (Department *) [self.treeData objectAtIndex:indexPath.row];
        if (!department.isLeaf) {
            NSArray *departments = [department departments];
            NSArray *employees = [department employees];
            BOOL isAlreadyInserted = NO;
            for (NSMutableArray<Department *> *item in department.departments) {
                NSInteger index = [self.treeData indexOfObjectIdenticalTo:item];
                isAlreadyInserted = (index > 0 && index != NSIntegerMax);
                if (isAlreadyInserted) {
                    break;
                }
            }

            for (NSMutableArray<Employee *> *item in department.employees) {
                NSInteger index = [self.treeData indexOfObjectIdenticalTo:item];
                isAlreadyInserted = (index > 0 && index != NSIntegerMax);
                if (isAlreadyInserted) {
                    break;
                }
            }

            if (isAlreadyInserted) {
                [self collapse:departments];
                [self removeEmployees:employees];
            } else {
                NSUInteger count = indexPath.row + 1;
                NSMutableArray *indexPaths = [NSMutableArray array];
                for (NSDictionary *inner in departments) {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                    [self.treeData insertObject:inner atIndex:count++];
                }

                for (NSDictionary *inner in employees) {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                    [self.treeData insertObject:inner atIndex:count++];
                }
                [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
    } else if ([[self.treeData objectAtIndex:indexPath.row] isKindOfClass:[Employee class]]) {
        Employee *employee = (Employee *) [self.treeData objectAtIndex:indexPath.row];
        DetailViewController *viewController = [DetailViewController initWithIdentifier:self.storyboard andModel:employee];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)collapse:(NSArray *)array {
    for (NSDictionary *inner in array) {
        NSUInteger indexToRemove = [self.treeData indexOfObjectIdenticalTo:inner];
        Department *department = (Department *) inner;
        NSArray *departments = [department departments];
        NSArray *employees = [department employees];
        if ([departments count] > 0) {
            [self collapse:departments];
        }

        if ([employees count] > 0) {
            [self removeEmployees:employees];
        }

        if ([self.treeData indexOfObjectIdenticalTo:inner] != NSNotFound) {
            [self.treeData removeObjectIdenticalTo:inner];
            [self.uiTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
        }
    }
}

- (void)removeEmployees:(NSArray *)array {
    for (NSDictionary *inner in array) {
        NSUInteger indexToRemove = [self.treeData indexOfObjectIdenticalTo:inner];
        if ([self.treeData indexOfObjectIdenticalTo:inner] != NSNotFound) {
            [self.treeData removeObjectIdenticalTo:inner];
            [self.uiTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Logout", @"") style:UIBarButtonItemStyleDone target:self action:@selector(logout:)];
    self.navigationItem.rightBarButtonItem = logoutButton;
}

- (IBAction)logout:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"password"];
    LoginViewController *viewController = [LoginViewController initWithIdentifier:self.storyboard];
    [self.navigationController setViewControllers:@[viewController] animated:YES];
}
@end
