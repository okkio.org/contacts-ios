//
//  LoginViewController.m
//  Contacts
//
//  Created by Павел on 02/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
@property(nonatomic, copy) NSString *username;
@property(nonatomic, copy) NSString *password;
@property(nonatomic, weak) IBOutlet UITextField *usernameView;
@property(nonatomic, weak) IBOutlet UITextField *passwordView;

- (IBAction)signIn:(id)sender;

@end

@implementation LoginViewController

static NSString *const VIEW_IDENTIFIER = @"loginView";

+ (id)initWithIdentifier:(UIStoryboard *)storyboard {
    LoginViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:VIEW_IDENTIFIER];
    return viewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    // @todo: implement Keychain
    self.username = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    self.password = [[NSUserDefaults standardUserDefaults] valueForKey:@"password"];
    [self login];
}

- (IBAction)signIn:(id)sender {
    self.username = self.usernameView.text;
    self.password = self.passwordView.text;
    [self login];
}

- (void)login {
    if ([self.username length] == 0 || [self.password length] == 0) {
        return;
    }

    NetManager *nm = [NetManager initWithUsername:self.username andPassword:self.password];
    [nm hello:^(BOOL result, NSString *message) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (result) {
                [[NSUserDefaults standardUserDefaults] setValue:self.username forKey:@"username"];
                [[NSUserDefaults standardUserDefaults] setValue:self.password forKey:@"password"];
                MainViewController *viewController = [MainViewController initWithIdentifier:self.storyboard];
                [self.navigationController setViewControllers:@[viewController] animated:YES];
            } else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", @"") message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                }];
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        });
    }];
}
@end
