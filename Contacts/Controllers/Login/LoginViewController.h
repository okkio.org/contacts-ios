//
//  LoginViewController.h
//  Contacts
//
//  Created by Павел on 02/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "NetManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController
+ (id)initWithIdentifier:(UIStoryboard *)storyboard;
@end

NS_ASSUME_NONNULL_END
