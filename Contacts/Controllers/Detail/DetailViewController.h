//
//  DetailViewController.h
//  Contacts
//
//  Created by Павел on 03/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Employee.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : UIViewController
+ (id)initWithIdentifier:(UIStoryboard *)storyboard andModel:(Employee *)model;
@end

NS_ASSUME_NONNULL_END
