//
//  DetailViewController.m
//  Contacts
//
//  Created by Павел on 03/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property(nonatomic, strong) Employee *employee;
@property(nonatomic, strong) UIImage *originalBackground;

@property(nonatomic, weak) IBOutlet UILabel *mobileView;
@property(nonatomic, weak) IBOutlet UILabel *emailView;
@property(nonatomic, weak) IBOutlet UILabel *fullNameView;
@property(nonatomic, weak) IBOutlet UILabel *userTitleView;
@property(nonatomic, weak) IBOutlet UIImageView *photoView;
@property(nonatomic, weak) IBOutlet UIButton *mobileButton;
@property(nonatomic, weak) IBOutlet UIButton *emailButton;

- (IBAction)doCall:(id)sender;

- (IBAction)doSendEmail:(id)sender;

@end

@implementation DetailViewController

static NSString *const VIEW_IDENTIFIER = @"detailView";
UIActivityIndicatorView *activityIndicator;

+ (id)initWithIdentifier:(UIStoryboard *)storyboard andModel:(Employee *)model {
    DetailViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:VIEW_IDENTIFIER];
    viewController.employee = model;
    return viewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavbar];

    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = self.photoView.center;
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];

    self.fullNameView.text = self.employee.name;
    self.userTitleView.text = self.employee.title;

    if (!self.employee.phone) {
        self.mobileView.hidden = YES;
        self.mobileButton.hidden = YES;
    } else {
        [self.mobileButton setTitle:self.employee.phone forState:UIControlStateNormal];
    }
    if (!self.employee.email) {
        self.emailView.hidden = YES;
        self.emailButton.hidden = YES;
    } else {
        [self.emailButton setTitle:self.employee.email forState:UIControlStateNormal];
    }

    self.photoView.layer.borderWidth = 3.0f;
    self.photoView.layer.borderColor = [UIColor whiteColor].CGColor;

    self.photoView.layer.cornerRadius = self.photoView.frame.size.width / 2;
    self.photoView.clipsToBounds = YES;

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSData *data = [[NSData alloc] initWithContentsOfURL:self.employee.photoUrl];
        if (data == nil) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [activityIndicator stopAnimating];
            self.photoView.image = [UIImage imageWithData:data];
        });
    });
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.shadowImage = self.originalBackground;
    self.navigationController.navigationBar.translucent = NO;
}

- (void)setupNavbar {
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
    self.originalBackground = [UIImage alloc];
    self.originalBackground = self.navigationController.navigationBar.shadowImage;

    // прозрачный navbar
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
}

- (IBAction)doCall:(id)sender {
    NSString *urlString = [[NSString alloc] initWithFormat:@"tel:%@", self.employee.phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString] options:@{} completionHandler:nil];
}

- (IBAction)doSendEmail:(id)sender {
    NSString *urlString = [[NSString alloc] initWithFormat:@"mailto:%@", self.employee.email];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString] options:@{} completionHandler:nil];
}
@end
