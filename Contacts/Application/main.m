//
//  main.m
//  Contacts
//
//  Created by Павел on 02/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
