//
//  AppDelegate.h
//  Contacts
//
//  Created by Павел on 02/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;

@property(readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

