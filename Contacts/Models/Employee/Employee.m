//
//  Employee.m
//  Contacts
//
//  Created by Павел on 06/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import "Employee.h"

@implementation Employee

- (id)initFromDictionary:(NSDictionary *)dictionary withLevel:(NSNumber *)level {
    self = [super init];
    if (self) {
        self.id = dictionary[@"ID"];
        self.name = dictionary[@"Name"];
        self.title = dictionary[@"Title"];
        self.email = dictionary[@"Email"];
        self.phone = dictionary[@"Phone"];
        self.level = level;
        self.leaf = YES;
        self.level = level;
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Name: %@", self.name];
}
@end
