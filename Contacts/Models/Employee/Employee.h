//
//  Employee.h
//  Contacts
//
//  Created by Павел on 06/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Employee : NSObject
@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSNumber *level;
@property(nonatomic, assign, getter = isLeaf) BOOL leaf;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *email;
@property(nonatomic, copy) NSString *phone;
@property(nonatomic, strong) NSURL *photoUrl;

- (id)initFromDictionary:(NSDictionary *)dictionary withLevel:(NSNumber *)level;
@end
