//
//  Department.m
//  Contacts
//
//  Created by Павел on 06/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import "Department.h"

@implementation Department
- (BOOL)isLeaf {
    if ([self.departments count] == 0 && [self.employees count] == 0) {
        return YES;
    }
    return NO;
}

- (Department *)init:(NSNumber *)id withName:(NSString *)name level:(NSNumber *)level {
    self = [super init];
    if (self) {
        self.id = id;
        self.name = name;
        self.level = level;
        self.departments = [NSMutableArray array];
        self.employees = [NSMutableArray array];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Name: %@", self.name];
}
@end
