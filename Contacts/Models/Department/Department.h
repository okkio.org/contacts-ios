//
//  Department.h
//  Contacts
//
//  Created by Павел on 06/12/2018.
//  Copyright © 2018 Pavel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Employee/Employee.h"

@interface Department : NSObject
@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSNumber *level;
@property(nonatomic, assign, getter = isLeaf) BOOL leaf;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, strong) NSMutableArray<Department *> *departments;
@property(nonatomic, strong) NSMutableArray<Employee *> *employees;

- (Department *)init:(NSNumber *)id withName:(NSString *)name level:(NSNumber *)level;
@end
